# -*- coding: utf-8 -*-

import os
import sys
import random
import datetime
import operator
import json
from plugins_core.command_plugin import CommandPlugin

__version__ = '0.0.1'

class Plugin(CommandPlugin):
    keys = ['grpwathc', u'статус_группы']
    name = u'Слежение за группой'
    info = u'Слежение за группой'

    version = __version__



    def __get_members(self,group,offset,count):

    	GetMembersRequest = {
        'group_id' : group,
        'sort' : 'id_asc',
        'offset' : offset,
        'count' : count,
        'fields' : 'bdate',
        }

        return self.vk.method('groups.getMembers', GetMembersRequest)

    def __add_fields(self, item):
    	item['bdate_list'] = []
       	item['is_member'] = True


    def __database_init(self, mlist, grp):

    	print(u'DB init.')

    	outputfilename = 'database'+str(grp)

    	for item in mlist:
    		self.__add_fields(item)     		

       	with open(outputfilename+'.json', 'wb') as outfile:
       		json.dump(mlist, outfile)

    def __build_dict(self, seq, key):
    	return dict((d[key], dict(d, index=index)) for (index, d) in enumerate(seq))


    def call(self):
        
        # ID группы, в которой искать
        #grp_id = 103628621
        #grp_id = 36655707
        #grp_id = 29392291 # Yuki Kajiura
        grp_id = 24739958 # TP ZP
        pack = 900

        outputfilename = 'ololo'

        # Первый запрос, что бы получить количество участников группы
        members_count = self.__get_members(grp_id,0,0)['count']

        print (u'Members count is ' + str(members_count))
        
        members_list = []

        # Допустим, во время запросов количество не поменяется >.<

        for i in range(0,members_count/pack):
        	print(u'Pack #' + str(i) + u'. From ' + str(pack*i) + u' to '+ str(pack*(i+1)-1) + u'.')
        	members_list.extend(self.__get_members(grp_id,pack*i,pack)['items'])

        print(u'Last pack. From ' + str((members_count/pack)*pack) + u' to '+ str((members_count/pack)*pack + members_count%pack - 1) + u'.')
        members_list.extend(self.__get_members(grp_id,(members_count/pack)*pack,members_count%pack)['items'])

       	print len(members_list)

       	# Список ID текущих участников
       	ids_now_list = []
       	for item in members_list:
       		ids_now_list.append(item['id'])
       		if not 'bdate' in item:
       			item['bdate']=''

        filename = 'database'+str(grp_id)+'.json'

       	if not os.path.isfile(filename):
       		self.__database_init(members_list,grp_id)

       	with open(filename) as data_file:
       		database = json.load(data_file)

       	print len(database)

       	was_members = []

       	# Список ID участников группы из БД, которые были в группе на момент обновления БД
       	for item in database:
       		if item['is_member']:
       			was_members.append(item['id'])

       	# Теперь нужно получить два списка - список внось прибывших и список покинувших.

       	# Вступившие "что сейчас - были"

       	new_members = list(set(ids_now_list)-set(was_members))
       	print new_members

       	# Покинувшие "были - что сейчас"
      	exit_members = list(set(was_members)-set(ids_now_list))
      	print exit_members
      	
      	# Создаём словарь для поиска в списке текущих участников по ID
      	mb_dict = self.__build_dict(members_list, key="id")

        # Создаём словарь для поиска в БД участников по ID
        db_dict = self.__build_dict(database, key="id")
      	
        # Добавляем в БД прибывших.
      	for item in new_members:
          if not item in db_dict:
            new_memb = members_list[mb_dict[item]['index']]
            self.__add_fields(new_memb)
            database.append(new_memb)
          else:
            database[db_dict[item]['index']]['is_member'] = True

      	# Создаём словарь для поиска в БД участников по ID
      	db_dict = self.__build_dict(database, key="id")

      	for item in exit_members:
      		database[db_dict[item]['index']]['is_member'] = False # Помечаем, что участник вышел, но не удаляем его

      	# Ищем тех, кто сменил дату (или удалил). Оповещаем про это и пишем в базу новую дату (если она новая).
        change_bdate = []
      	for member in members_list:
          if member['bdate'] != db_dict[member['id']]['bdate'] :
            if (member['bdate'] != '') and (member['bdate'] not in db_dict[member['id']]['bdate_list']):
              database[db_dict[member['id']]['index']]['bdate_list'].append(member['bdate'])
            database[db_dict[member['id']]['index']]['bdate'] = member['bdate']
            print(u'Member ID ' + str(member['id']) + u' has change birthday date.')
            change_bdate.append(member['id'])

        # Пишем на диск
      	with open(filename, 'wb') as outfile:
       		json.dump(database, outfile)





       	





    # Следить за пришедшими/ушедшими
    # Держать таблицу с днями рождения. 
    # Запоминать если кто-то изменил, удалил. Как-то хранить несколько вариантов, которые были.
    
        