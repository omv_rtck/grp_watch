# -*- coding: utf-8 -*-
from vkservice import VkServiceClient

class CommandPlugin:
    vk = None
    vks = None

    plugin_type = 'command'
    name = 'command plugin'

    def __init__(self, vk):
        self.vk = vk
        self.vks = VkServiceClient()

    def call(self, msg):
      print(msg)