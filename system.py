# -*- coding: utf-8 -*-

import os
import sys
import signal
import time
import imp
import yaml
import glob

from multiprocessing import Process, Queue
from vkservice import VkServiceServer, VkServiceClient

from grpwatch_core import Plugin

__version__ = '0.2.0'

class BotCore:

    # -Вынести в модуль "commands"-
    # Нет, пока что здесь остаються.
    commands = {}
    plugins = {}
    plugin = None
    messagehandlers = {}
    lastmessid = 0

    core_exit_run = False
    core_restart = False
    core_kill_signal = False

    # константы
    be_online = False

    version = __version__

    def __init__(self, settings):
        path = settings['path']
        self.cmd_in_own_messages = settings['cmd_in_own_messages']

        if settings.has_key('be_online'):
            self.be_online = settings['be_online']
        
        self.settings = settings

        print(u'Python social network bot.')

        print(u'VK.COM autorization...')

        
        # Логины и пароли вынести в отдельный файл. Что бы всем подряд модулям не передавать.
        self.vkapi_service = VkServiceServer(settings['vk_login'], settings['vk_password'], settings['vk_app_id'])
        self.vkclient = VkServiceClient()

        print(u'Loading plugins...')

        print(u'---------------------------')

        # Подгружаем модули
        # Это вынести как класс - загрузчик модулей(плагинов). 
        # И тут же грузить модули. У модуля будут свои плагины. 


        self.plugin = Plugin(self.vkclient)
        
        print(u'---------------------------')

    def run(self):
  
        proc = Process(target=self.vkapi_service.run, args=())
        proc.start()

        print(u'Приступаю к приему сообщений')    

        # Нужно вынести в отдельную функцию. Возможно.
        # Добавить обработку своих сообщений. Что бы бот мог работать на странице хозяина и реагировать на его запросы. 
        self.plugin.call()


        print(u'Exit by `core_exit_run`.')
        self.core_kill_signal = False

        print(u'self.vkclient.send_exit()')
        self.vkclient.send_exit()
        print(u'wait proc exit')
        proc.join()

        


    # скорее всего переделать. self.command реализовать во внешнем модуле. 
    # parce(out) Читает сообщения из вк. Параметром out задаётся читать входящие или исходящие сообщения. 
    # Дальше передаёт сообщения обработчикам. Ничего не возвращает.
    def parce(self, out):
        # Скрытый баг! Связанный с работой метода vk api
        #
        # Если одновременно обратяться больше 20 пользователей, все кто 21-й и выше - будут проигнорированны.
        #
        # https://vk.com/dev/messages.get :
        # идентификатор сообщения, полученного перед тем, которое нужно вернуть последним (при условии, 
        # что после него было получено не более count сообщений, иначе необходимо использовать с параметром offset).
        # ** конец цитаты описания
        # Варианты режения
        # Смотреть разницу запомненого self.lastmessid и полученного после выполнения запроса. 
        # Если больше 20, ... :
        # 1. ..., то self.lastmessid сдвинуть только на 20.
        #    ? Не знаю как будет работать при переключении out=0 out=1 ?
        # 2. ..., закинуть полученные ответы в отдельный список, сделать ещё запросы с изменением offset, 
        #    добавляя сообщения в этот список.
        values = {
            'out': out,
            'offset': 0,
            'count': 20,
            'time_offset': 50,
            'filters': 0,
            'preview_length': 0,
            'last_message_id': self.lastmessid
        }

        # На будущее: обращаться не на прямую к vk_api, а через "очереди" или что-то пободное
        response = self.vkclient.method('messages.get', values)

        if response['items']:
            self.lastmessid = response['items'][0]['id']
            for message in response['items']:          
                self.message_handler(message)

        time.sleep(0.34)

    # реализовать во внешнем модуле Oo или как?. (наверное там же где и self.command) 
    def commadn_execute(self, message, plugin, params):

        if plugin and plugin in self.commands:
            # Помечаем прочитанным перед выполнением команды.
            # Может быть вынести это ниже? Так он себя очень скрытно ведёт,
            # на несуществующие команды никак не реагирует.             
            self.vkclient.markasread(message['id']) 

            # Приоритеты аргументов:
            # 0. message - всегла есть.
            # 1. params, with_args, args
            # 2. system

            # 0
            args = [message]

            # 1
            if any(s in self.commands[plugin].plugin_type for s in ['params', 'with_args', 'args']):
                args.append(params)
            # 2
            if 'system' in self.commands[plugin].plugin_type:
                args.append(self)

            self.commands[plugin].call(*args)
            return True
        else:
            return False

    # Получает сообщение. Передаёт каждому обработчику из списка, пока один 
    # из них не обработает сообщение. Или пока не закончиться список. 
    def message_handler(self, message):
        for handler in self.messagehandlers.values():
            result = handler.parce(message)

            # проверяем подходит ли сообщение под условия обработчика
            if result.has_key('plugin'):
                # Совпало - запускаем команду.
                self.commadn_execute(message, result['plugin'], result['params'])
                return True

        # ни с чем не совпало
        return False

    def terminate(self, signal, frame):
        print(u'signal::'+str(signal))
        # На случай если программа зависла где-то там, где нет слежения за self.core_exit_run
        if self.core_kill_signal:
            # При получении второго сигнала выходим  
            print(u'Second signal. Exiting')
            sys.exit(0)
        else:
            # При получении первого сигнала - даём завершиться правильно.
            self.core_exit_run = True
            self.core_kill_signal = True
        

if __name__ == '__main__':
    with open('settings.yml', 'r') as file:
        settings = yaml.load(file)

    core = BotCore(settings)
    #Ctrl-C
    signal.signal(signal.SIGINT, core.terminate)
    #kill 
    signal.signal(signal.SIGTERM, core.terminate)

    core.run()
    if core.core_restart:
        print(u'Перезапуск...')
        # Затычка что бы цыклически не перезагружалось
        time.sleep(51)
        print(u'Перезапуск!')
        sys.exit(1)
    else:
        print(u'Выход')
        sys.exit(0)
