# -*- coding: utf-8 -*-
# Класс с некоторыми доп. методами 

import pickle
import json
import zmq

from vkplus import VkPlus


class VkServiceServer:

    exit = False

    def __init__(self, vk_login, vk_pass, vk_app_id):
        self.vkplus = VkPlus(vk_login, vk_pass, vk_app_id)
        # нужно выделить логин в отдельную функцию

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind('tcp://127.0.0.1:43000') # вынести в параметры

        while not self.exit:
            try:
                command, key, data = pickle.loads(socket.recv())

                if command == 'respond':
                    result = self.vkplus.respond(key, data)                  
                    socket.send(pickle.dumps(result))
                elif command == 'markasread':
                    result = self.vkplus.markasread(key) 
                    socket.send(pickle.dumps(result))
                elif command == 'method':
                    result = self.vkplus.api.method(key, data)
                    socket.send(pickle.dumps(result))
                elif command == 'exit':
                    self.exit = True
                    socket.send(b'ok')

            except Exception as e:
                print(e)


class VkServiceClient:

    def __init__(self):       
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect('tcp://127.0.0.1:43000')

    def respond(self, key, data):
        self.socket.send(pickle.dumps(('respond', key, data)))
        return pickle.loads(self.socket.recv())

    def markasread(self, key):
        self.socket.send(pickle.dumps(('markasread', key, None)))
        return pickle.loads(self.socket.recv())

    def method(self, key, data=None):
        self.socket.send(pickle.dumps(('method', key, data)))
        return pickle.loads(self.socket.recv())

    def send_exit(self):
        self.socket.send(pickle.dumps(('exit', None, None)))
        return self.socket.recv() == b'ok'